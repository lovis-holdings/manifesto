# LOVIS Manifesto

> Why do we never have time to do it right, but always have time to do it over?

Este manifiesto es una mezcla de ideas para unificar e integrar a los equipos de trabajo en LOVIS. Las ideas plasmadas aquí son el reflejo de las opiniones combinadas, debatidas y refinadas de todo aquel en LOVIS que tenga el coraje para compartirlas.

## Perfil LOVIS

#### 🤓 Autodidacta
>  Saber como resolver problemas, tener **hambre por las respuestas correctas**, ser flexible, multidisciplinario, ágil para aprender cosas nuevas y mejores.

-------

#### 🙋‍♂️ Proactivo
> Ser intuitivo, sin miedo a expresarse, tener ganas de materializar las ideas, **querer marcar la diferencia**, ver las cosas desde una perspectiva nueva.

-------

#### 👨‍🎓 Humilde
> Dejar atrás la arrogancia, entender que hacer algo diferente no es igual a incorrecto, tener la **paciencia para ser un alumno** y el conocimiento para ser un mentor, sin miedo a estar equivocado, sin miedo preguntarle a los demás.

-------

#### 👨‍💻 Auto-organizado
> Saber administrar el tiempo, ser astuto sobre la división del trabajo, dar la correcta prioridad a las tareas, tener **conciencia sobre las metas a cumplir** y las metas no cumplidas.

-------

## Prácticas LOVIS

#### Desarrollo
* [x] [Workflow](Workflow.md)
* [x] [Commits](Commits.md)
* [x] [Issues](Issues.md)
* [x] [Styles](Styles.md)
* [ ] Documentación
* [x] [Formato de React](React.md)

#### Diseño
* [ ] Workflow

-------
[😎](Pared.md)
