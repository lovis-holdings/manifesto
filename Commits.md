# Commits

#### Commit messages

> Keep your own history readable.

> Some people do this by just working things out in their head first, and not making mistakes. But that’s very rare, and for the rest of us, we use “git rebase” etc. while we work on our problems.

> Don’t expose your crap.

#### Formato

El título de un commit debe ser directo y corto (de preferencia menos de 50 caracteres), la descripción servirá solo para dar detalles extra de la implementación de los cambios. No todos los commits requieren descripción, especialmente si el título del commit resume perfectamente los cambios realizados al repositorio.

##### Prefijos

👾 -> *Chore*

🙌 -> *New Feature*

🐞 -> *Bug Fix*

📄 -> *Documentation*

💅 -> *Code Style*

🎛 -> *Refactor*

🔧 -> *Performance*

⚗️ -> *Tests*

🚦 -> *Build*

🤖 -> *Continous Integration*

#### Issues Addressing 

Si un commit resuelve o hace referencia a alguno de los issues reportados en el board deberás agregar en el titulo el simbolo '#' y el numero del issue.

Ejemplos:

```
🐞 - Firebase login crash.

* Angular 4.2.1 fixes issues with AngularFire 3.1.2
```

```
🙌 - Apple Pay View Controllers
```
```
🐞 - #15 Nested renaming at main model
```


 

#### Herramientas recomendadas

* Git Flow
* Git Kraken u otro cliente visual de git


