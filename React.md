#  Guía de Estilo React / JSX

_Un enfoque razonable para React y JSX_

## Table de contenido

1. [Reglas básicas](#reglas-básicas)
1. [Class vs `React.createClass` vs stateless](#class-vs-reactcreateclass-vs-stateless)
1. [Nomenclatura](#nomenclature)
1. [Declaraciones](#declaraciones)
1. [Indentación](#indentacion)
1. [Comillas](#comillas)
1. [Espaciado](#espaciado)
1. [Props](#props)
1. [Refs](#refs)
1. [Parentesis](#parentesis)
1. [Tags](#tags)
1. [Metodos](#metodos)
1. [Orden](#ordering)
1. [`isMounted`](#ismounted)

## Reglas básicas

- Incluye sólo un componente React por archivo.
- Sin embargo, se admiten múltiples [Stateless, o Pure, Components](https://facebook.github.io/react/docs/reusable-components.html#stateless-functions) por archivo. eslint: [`react/no-multi-comp`](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-multi-comp.md#ignorestateless).
- Siempre usa la sintaxis de JSX.
- No utilices `React.createElement` a menos que estés inicializando la aplicación desde un archivo que no sea JSX.

## Class vs `React.createClass` vs stateless

- Si el componente tiene estado y/o refs, elija `class extends Component` sobre` React.createClass` a menos que tenga una buena razón para usar mixins.
eslint: [`react/prefer-es6-class`](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/prefer-es6-class.md) [`react/prefer-stateless-function`](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/prefer-stateless-function.md)


    Bien ✅
    ```jsx
    class Listing extends Component {
      // ...
      render() {
        return <div>{this.state.hello}</div>
      }
    }
    ```

    Mal ❌
    ```jsx
    const Listing = React.createClass({
      // ...
      render() {
        return <div>{this.state.hello}</div>
      }
    })
    ```

    Y si no tienes estado o refs, elije funciones normales y no arrow functions en las clases:

    Bien ✅
    ```jsx
    function Listing({ hello }) {
      return <div>{hello}</div>
    }
    ```

    Mal ❌
    ```jsx
    class Listing extends Component {
      render() {
        return <div>{this.props.hello}</div>
      }
    }
    ```

    Mal ❌
    No es bueno confiar en el a inferencia del nombre de la función
    ```jsx
    const Listing = ({ hello }) => (
      <div>{hello}</div>
    )
    ```

## Nomenclatura

- __Extensiones__: usa la extension `.jsx` para componentes React.
- __Nombre de archivos__: usa PascalCase para el nombre de los archivos. Ejemplo, `ReservationCard.jsx`.
- __Nomenclature de referencia__: usa PascalCase para componentes React y camelCase para sus instancias. Por convención, react usa la distinción mayúscula-minúscula para distinguir clases de HTML tags. eslint: [`react/jsx-pascal-case`](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-pascal-case.md).

    Bien ✅
    ```jsx
    import ReservationCard from './ReservationCard'
    ```

    Bien ✅
    ```jsx
    const reservationItem = <ReservationCard />
    ```

    Mal ❌
    ```jsx
    import reservationCard from './ReservationCard'
    ```
    Mal ❌
    ```jsx
    const ReservationItem = <ReservationCard />
    ```

- __Nomenclatura de los componentes__: usa el nombre de archivo como el nombre del componente. Por ejemplo, `ReservationCard.jsx` debe tener una referencia tal como `ReservationCard`. Sin embargo, para el componente raíz de un directorio, usa `index.js` como el nombre del archivo y además usa el nombre del directorio como el nombre del componente:

    Bien ✅
    ```jsx
    import Footer from './Footer'
    ```

    Mal ❌
    ```jsx
    import Footer from './Footer/index'
    ```

    Mal ❌
    ```jsx
    import Footer from './Footer/Footer'
    ```

- __Nomenclatura HOC (High order component)__: Componga el nombre del high order component y el nombre del component como `displayName` en el nuevo componente generado. Por ejemplo, el componente de orden superior `withFoo()`, al pasar un componente `Bar` debería producir un componente con` displayName` de `withFoo(Bar)`.

    > ¿Por qué? El `displayName` de un componente puede ser utilizado por herramientas de desarrollo o en mensajes de error, y tener un valor que exprese claramente esta relación ayuda a las personas a entender lo que está sucediendo.

    Bien ✅
    ```jsx
    export default function withFoo(WrappedComponent) {
      function WithFoo(props) {
        return <WrappedComponent {...props} foo />
      }

      const wrappedComponentName = WrappedComponent.displayName || WrappedComponent.name || 'Component'

      WithFoo.displayName = `withFoo(${wrappedComponentName})`
      return WithFoo
    }
    ```

    Mal ❌
    ```jsx
    export default function withFoo(WrappedComponent) {
      return function WithFoo(props) {
        return <WrappedComponent {...props} foo />
      }
    }
    ```

- __Nombres de props__:  Evita usar nombres de props de componentes de DOM para diferentes propósitos.

    > ¿Por qué? La gente espera props como `style` y `className` signifiquen una cosa específica. La variación de esta API para una parte de la aplicación hace que el código sea menos legible y menos mantenible, y podría causar errores.

    Bien ✅
    ```jsx
    <MyComponent variant="fancy" />
    ```
    Mal ❌
    ```jsx
    <MyComponent style="fancy" />
    ```


## Declaraciones

- No usa `displayName` para nombrar componentes. En cambio, nombre los componentes por referencia.

Bien ✅
```jsx
class ReservationCard extends Component {
  ...
}

export default ReservationCard
```

Mal ❌
```jsx
export default React.createClass({
  displayName: 'ReservationCard',
  ...
})
```

## Indentación

- Siga estos estilos de indentación para la sintaxis de JSX. eslint: [`react/jsx-closing-bracket-location`](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-closing-bracket-location.md)
    - Si los props caben en una linea, mantén todo en la misma linea
    - Si Los props están en múltiples líneas, deben estar alineados, cada uno debe ir en una linea y el tag de cierre debe estar alineado con el de apertura

    Bien ✅
    ```jsx
    <Foo bar="bar" />
    ```

    Bien ✅
    ```jsx
    <Foo
      superLongParam="bar"
      anotherSuperLongParam="baz"
    />

    // children get indented normally
    <Foo
      superLongParam="bar"
      anotherSuperLongParam="baz"
    >
      <Quux />
    </Foo>
    ```

    Mal ❌
    ```jsx
    <Foo superLongParam="bar"
         anotherSuperLongParam="baz" />
    ```

    Mal ❌
    ```jsx
    <Foo
      superLongParam="bar"
      anotherSuperLongParam="baz" />
    ```
    ```

    Mal ❌
    ```jsx
    <Foo superLongParam="bar" anotherSuperLongParam="baz"
    />
    ```

## Comillas

- Siempre usa comillas dobles (`"`) para los atributos JSX, pero comillas simples (`'`) para otros JS. eslint: [`jsx-quotes`](http://eslint.org/docs/rules/jsx-quotes)

    > ¿Por qué? Los atributos HTML suelen usar comillas dobles en lugar de simples, por lo que los atributos JSX reflejan esta convención.

    Bien ✅
    ```jsx
    <Foo bar="bar" />
    ```

    Bien ✅
    ```jsx
    <Foo style={{ left: '20px' }} />
    ```

    Mal ❌
    ```jsx
    <Foo bar='bar' />
    ```

    Mal ❌
    ```jsx
    <Foo style={{ left: "20px" }} />
    ```

## Espaciado

- Nunca incluir un espacio para separar los dos caracteres de los tokens `</` y `/>`

    Bien ✅
    ```jsx
    <App/>
    <input/>
    <Provider></Provider>
    ```

    Mal ❌
    ```jsx
    <App/ >
    <input/
    >
    <Provider>< /Provider>
    ```

- Siempre incluir un espacio en los self-closing tags. eslint: [`no-multi-spaces`](http://eslint.org/docs/rules/no-multi-spaces), [`react/jsx-tag-spacing`](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-tag-spacing.md)

    Bien ✅
    ```jsx
    <Foo />
    ```

    Mal ❌
    ```jsx
    <Foo/>
    ```

    Muy mal ❌
    ```jsx
    <Foo                 />
    ```

    Mal ❌
    ```jsx
    <Foo
     />
    ```

- No uses llaves en JSX con espacios en el medio. eslint: [`react/jsx-curly-spacing`](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-curly-spacing.md)

    Bien ✅
    ```jsx
    <Foo bar={baz} />
    ```

    Mal ❌
    ```jsx
    <Foo bar={ baz } />
    ```

## Props

- Siempre usa camelCase para los nombres de las props.

    Bien ✅
    ```jsx
    <Foo
      usarName="hello"
      phoneNumber={12345678}
    />
    ```

    Mal ❌
    ```jsx
    <Foo
      usarName="hello"
      phone_number={12345678}
    />
    ```

- Omite el valor de la prop cuando esta sea explícitamente `true`. eslint: [`react/jsx-boolean-value`](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-boolean-value.md)

    Bien ✅
    ```jsx
    <Foo
      hidden
    />
    ```

    Mal ❌
    ```jsx
    <Foo
      hidden={true}
    />
    ```

- Incluye información de accesibilidad, como `alt`, `aria-label`, `aria-labelledby` o `title` en componentes `<img />` `<object />`, `<area />` y `<input />`. Este componente es crítico para  eslint: [`jsx-a11y/alt-text`](https://github.com/evcohen/eslint-plugin-jsx-a11y/blob/master/docs/rules/alt-text.md)

    Bien ✅
    ```jsx
    <img src="foo" alt="Foo eating a sandwich." />
    <img src="foo" alt={"Foo eating a sandwich."} />
    <img src="foo" alt={altText} />
    <img src="foo" alt={`${person} smiling`} />
    <img src="foo" alt="" />

    <object aria-label="foo" />
    <object aria-labelledby="id1" />
    <object>Meaningful description</object>
    <object title="An object" />

    <area aria-label="foo" />
    <area aria-labelledby="id1" />
    <area alt="This is descriptive!" />

    <input type="image" alt="This is descriptive!" />
    <input type="image" aria-label="foo" />
    <input type="image" aria-labelledby="id1" />
    ```

    Mal ❌
    ```jsx
    <img src="foo" />
    <img {...props} />
    <img {...props} alt /> // Has no value
    <img {...props} alt={undefined} /> // Has no value
    <img {...props} alt={`${undefined}`} /> // Has no value
    <img src="foo" role="presentation" /> // Avoid ARIA if it can be achieved without
    <img src="foo" role="none" /> // Avoid ARIA if it can be achieved without

    <object {...props} />

    <area {...props} />

    <input type="image" {...props} />
    ```

- No utilices palabras como "image", "photo" o "picture" en los tags `<img>` `alt`. eslint: [`jsx-a11y/img-redundant-alt`](https://github.com/evcohen/eslint-plugin-jsx-a11y/blob/master/docs/rules/img-redundant-alt.md)

    > ¿Por qué? Los lectores de pantalla ya anuncian elementos `img` como imágenes, por lo que no es necesario incluir esta información en el texto alternativo.

    Bien ✅
    ```jsx
    <img src="hello.jpg" alt="Me waving hello" />
    ```

    Mal ❌
    ```jsx
    <img src="hello.jpg" alt="Picture of me waving hello" />
    ```

- usa solamente [ARIA roles](https://www.w3.org/TR/wai-aria/roles#role_definitions). eslint: [`jsx-a11y/aria-role`](https://github.com/evcohen/eslint-plugin-jsx-a11y/blob/master/docs/rules/aria-role.md) validos y no abstractos

    Bien ✅
    ```jsx
    <div role="button" />
    ```

    Mal ❌
    ```jsx
    // abstract ARIA role
    <div role="range" />
    ```

    Mal ❌
    ```jsx
    // not an ARIA role
    <div role="datepicker" />
    ```

- No utilices `accessKey` en elementos. eslint: [`jsx-a11y/no-access-key`](https://github.com/evcohen/eslint-plugin-jsx-a11y/blob/master/docs/rules/no-access-key.md)

    > ¿Por qué? Ciertas inconsistencias entre shortcuts de teclado y comandos de teclado utilizados por personas que usan lectores de pantalla y teclados complican la accesibilidad.

    Bien ✅
    ```jsx
    <div />
    ```

    Mal ❌
    ```jsx
    <div accessKey="h" />
    ```

- Evita utilizar `key` como prop del indice del array, usa un ID único. [react/no-array-index-key](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-array-index-key.md) ([¿Por qué?](https://medium.com/@robinpokorny/index-as-a-key-is-an-anti-pattern-e0349aece318))

    Bien ✅
    ```jsx
    {todos.map(todo => (
      <Todo
        {...todo}
        key={todo.id}
      />
    ))}
    ```

    Mal ❌
    ```jsx
    {todos.map((todo, index) =>
      <Todo
        {...todo}
        key={index}
      />
    )}
    ```

- Siempre defina DefaultProps de forma explícita para todos los props no requeridos.

    > ¿Por qué? Los PropTypes son una forma de documentación, y proporcionar DefaultProps significa que el lector de su código no tiene tanto que asumir. Además, puede significar que su código puede omitir ciertas comprobaciones de tipo.

    Bien ✅
    ```jsx
    function SFC({ foo, bar }) {
      return <div>{foo}{bar}</div>
    }

    SFC.propTypes = {
      foo: PropTypes.number.isRequired,
      bar: PropTypes.string,
    }

    SFC.defaultProps = {
      bar: '',
      children: null,
    }
    ```

    Mal ❌
    ```jsx
    function SFC({ foo, bar, children }) {
      return <div>{foo}{bar}{children}</div>
    }

    SFC.propTypes = {
      foo: PropTypes.number.isRequired,
      bar: PropTypes.string,
      children: PropTypes.node,
    }
    ```

## Refs

- Siempre usa usa ref callbacks. eslint: [`react/no-string-refs`](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-string-refs.md)

    Bien ✅
    ```jsx
    <Foo
      ref={(ref) => { this.myRef = ref }}
    />
    ```

    Mal ❌
    ```jsx
    <Foo
      ref="myRef"
    />
    ```

## Paréntesis

- Rodea JSX tags con paréntesis cuando sean estén en mas de una linea. eslint: [`react/wrap-multilines`](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/wrap-multilines.md)

    Bien ✅
    ```jsx
    render() {
      return (
        <MyComponent className="long body" foo="bar">
          <MyChild />
        </MyComponent>
      )
    }
    ```

    Bien ✅
    ```jsx
    render() {
      const body = <div>hello</div>
      return <MyComponent>{body}</MyComponent>
    }
    ```

    Mal ❌
    ```jsx
    render() {
      return <MyComponent className="long body" foo="bar">
               <MyChild />
             </MyComponent>
    }
    ```

## Tags

- Siempre auto-cierra tags que no tienen children. eslint: [`react/self-closing-comp`](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/self-closing-comp.md)

    Bien ✅
    ```jsx
    <Foo className="stuff" />
    ```

    Mal ❌
    ```jsx
    <Foo className="stuff"></Foo>
    ```

## Metodos

- usa arrow functions.

    Bien ✅
    ```jsx
    function ItemList(props) {
      return (
        <ul>
          {props.items.map((item, index) => (
            <Item
              key={item.key}
              onClick={() => doSomethingWith(item.name, index)}
            />
          ))}
        </ul>
      )
    }
    ```

    Mal ❌
    ```jsx
    function ItemList(props) {
      return (
        <ul>
          {props.items.map(function(item, index) {
            return (
              <Item
                key={item.key}
                onClick={() => doSomethingWith(item.name, index)}
              />
            )
          })}
        </ul>
      )
    }
    ```

- Bindear eventos por cada render method en el constructor. eslint: [`react/jsx-no-bind`](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-no-bind.md)

    > ¿Por qué? Una llamada de bind en render crea una función nueva en cada render y esto afecta al performance.

    Bien ✅
    ```jsx
    class extends Component {
      constructor(props) {
        super(props)

        this.onClickDiv = this.onClickDiv.bind(this)
      }

      onClickDiv() {
        // do stuff
      }

      render() {
        return <div onClick={this.onClickDiv} />
      }
    }
    ```

    Bien ✅
    ```jsx
    // using autobind helper
    import autobind from 'class-autobind'

    class extends Component {
      constructor(props) {
        super(props)
        autobind(this)
      }

      onClickDiv() {
        // do stuff
      }

      render() {
        return <div onClick={this.onClickDiv} />
      }
    }
    ```

    Mal ❌
    ```jsx
    class extends Component {
      onClickDiv() {
        // do stuff
      }

      render() {
        return <div onClick={this.onClickDiv.bind(this)} />
      }
    }
    ```

- No utilices prefijo de _ (underscore) para los métodos internos de un componente React. eslint: [no-underscore-dangle](https://eslint.org/docs/rules/no-underscore-dangle)
    > ¿Por qué? Los prefijos _ (underscore) a veces se usan como una convención en otros idiomas para denotar privacidad. Pero, a diferencia de esas lenguajes, no hay soporte nativo para el ámbito en JavaScript ya que todo es público. Independientemente de sus intenciones, agregar prefijos a sus propiedades en realidad no los hacen privados, y cualquier property debe ser tratado como público. Ver errores en [#1024](https://github.com/airbnb/javascript/issues/1024), y [#490](https://github.com/airbnb/javascript/issues/490).

    Bien ✅
    ```jsx
    class extends Component {
      onClickSubmit() {
        ...
      }

      ...
    }
    ```

    Mal ❌
    ```jsx
    React.createClass({
      _onClickSubmit() {
        ...
      },

      ...
    })
    ```

- Asegúrate de regresar un valor en los métodos `render`. eslint: [`react/require-render-return`](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/require-render-return.md)

    Bien ✅
    ```jsx
    render() {
      return (<div />)
    }
    ```

    Mal ❌
    ```jsx
    render() {
      (<div />)
    }
    ```

## Orden

- Orden de `class extends Component`:

1. métodos opcionales `static`
1. `constructor`
1. `getChildContext`
1. `componentWillMount`
1. `componentDidMount`
1. `componentWillReceiveProps`
1. `shouldComponentUpdate`
1. `componentWillUpdate`
1. `componentDidUpdate`
1. `componentWillUnmount`
1. _clickHandlers or eventHandlers_ como `onClickSubmit()` o `onChangeDescription()`
1. _getter methods for `render`_ como `getSelectReason()` o `getFooterContent()`
1. _opcional render_ como `renderNavigation()` o `renderProfilePicture()`
1. `render`

- Cómo definir `propTypes`, `defaultProps`, `contextTypes`, etc...

    ```jsx
    import React from 'react'
    import PropTypes from 'prop-types'

    class Link extends Component {
      static methodsAreOk() {
        return true
      }

      render() {
        return <a href={this.props.url} data-id={this.props.id}>{this.props.text}</a>
      }
    }

    Link.propTypes = {
      id: PropTypes.number.isRequired,
      url: PropTypes.string.isRequired,
      text: PropTypes.string,
    }

    Link.defaultProps = {
      text: 'Hello World',
    }

    export default Link
    ```

__[⬆ Vuelve arriba](#table-of-contents)__
