# Styles

> Every line of code should appear to be written by a single person, no matter the number of contributors.

## Syntax

- Usa _soft tabs_ con 2 espacios. Es la única manera de garantizar que el código se muestra igual en cualquier ambiente.
- Cuando agrupes selectores, mentén selectores individuales en una línea.
- Incluye un espacio antes del `{` de un bloque de declaracin para legibilidad.
- Coloca el `}` de un bloque de declaracin en una nueva línea
- Incluye un espacio después de `:` para cada declaración.
- Cada declaracin debe aparecer en su propia línea para mejor reporteo de errores.
- Termina todas las declarationes con `;`. El último es opcional, pero tu código será propenso a errores sin el.
- Valores separados por comas deben incluir un espacio después de cada coma (ejemplo: box-shadow).
- No incluyas espacios después de comas en valores `rgb()`, `rgba()`, `hsl()`, `hsla()`, o `rect()`. Esto ayuda a diferenciar valores de colores (coma, sin espacio) de valores de propiedad (coma, con espacio).
- No antepongas un 0 en números decimales (ejemplo: `.5` en lugar de `0.5` y `-.5` en lugar de `-0.5`).
- Todos los valores hexadecimales (ejemplo: `#fff`) deben ir en minúsculas. Las letras minúsculas son más fáciles de discernir al escanear un documento ya que tienden a tener formas más únicas.
- Usa valores hexadecimales abreviados cuando estén disponibles (ejemplo: `#fda` en lugar de `#ffddaa`).
- Pon comillas en los valores de atributo de selectores (ejemplo: `input[type="text"]`). Son opcionales en la mayoría de los casos, y es una buena práctica para consistencia.
- No definas unidades para valores cero (ejemplo: `margin: 0;` en lugar de `margin: 0px;`)

### CSS malo ❌
```css
.selector, .selector-secondary, .selector[type=text] {
  padding:15px;
  margin:0px 0px 15px;
  background-color:rgba(0, 0, 0, 0.5);
  box-shadow:0px 1px 2px #CCC,inset 0 1px 0 #FFFFFF
}
```
### CSS bueno ✅
```css
.selector,
.selector-secondary,
.selector[type="text"] {
  padding: 15px;
  margin-bottom: 15px;
  background-color: rgba(0,0,0,.5);
  box-shadow: 0 1px 2px #ccc, inset 0 1px 0 #fff;
}
```

## Orden
Declaraciones de propiedad deben estar agrupadas en el siguiente orden:

1. Posición
2. _Box model_
3. Typografía
4. Visual

El posicionamiento viene antes porque puede remover un elemento del flujo normal del documento y sobreescribir estilos relacionados con el _box model_. El _box model_ viende después porque dicta las dimensiones y ubicación de un componente.

Todo lo demás toma lugar dentro del componente o no afecta las dos primeras secciones, por lo tanto, vienen al final.

```css
.declaration-order {
  /* Posición */
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 100;

  /* Box-model */
  display: block;
  float: right;
  width: 100px;
  height: 100px;

  /* Typografía */
  font: normal 13px "Helvetica Neue", sans-serif;
  line-height: 1.5;
  color: #333;
  text-align: center;

  /* Visual */
  background-color: #f5f5f5;
  border: 1px solid #e5e5e5;
  border-radius: 3px;

  /* Misc */
  opacity: 1;
}
```

## No uses `@import`s

Comparado con `<link>`s, `@import` es más lento, añade _requests_ extra y puede causar problemas imprevistos. Evítalos y opta por alguna alternativa:

- Usa múltiples `<link>`s
- Compila tu CSS con un preprocesador como SASS o LESS en un solo archivo
- Concatena tus archivos CSS con herramientas como las que proveen Rails, Jekyll, etc.

### Usa `<link>`s ✅
```html
<link rel="stylesheet" href="core.css">
```

### Evita `@import`s ❌
```html
<style>
  @import url("more.css");
</style>
```

Para más información, lee [este artículo](http://www.stevesouders.com/blog/2009/04/09/dont-use-import/)

## Ubicación de los `media query`s

Coloca `media query`s tan cerca de su regla relevante como sea posible. No los juentes todos en un archivo separado o al final del documento. Hacer esto hace que sea fácil que otras personas no se den cuenta que existen.

```css
.element { ... }
.element-avatar { ... }
.element-selected { ... }

@media (min-width: 480px) {
  .element { ...}
  .element-avatar { ... }
  .element-selected { ... }
}
```


## Propiedades con _vendor prefix_

Cuando uses propiedades con _vendor prefix_, indenta cada propiedad para que los valores se alinean verticalmente. Esto facilita la edición multi-línea.

```css
/* Prefixed properties */
.selector {
  -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.15);
          box-shadow: 0 1px 2px rgba(0,0,0,.15);
}
```


## Declaraciones simples

En instancias donde una regla contiene sólo una declaración, considera quitar saltos de línea para mayor legibilidad y rápida edición.

El factor clave aquí es detección de errores. Un validador CSS diciendo que tienes un error de sintáxis en la línea 183. Con una declaracin por línea, no hay pierde.

```css
/* Declaraciones simples en una línea */
.span1 { width: 60px; }
.span2 { width: 140px; }
.span3 { width: 220px; }

/* Declaraciones múltiples, una por línea */
.sprite {
  display: inline-block;
  width: 16px;
  height: 15px;
  background-image: url(../img/sprite.png);
}
.icon           { background-position: 0 0; }
.icon-home      { background-position: 0 -20px; }
.icon-account   { background-position: 0 -40px; }
```

## Notación abreviada (_Shorthand notation_)

Limita el uso de notaciones abreviadas para instancias donde debes definir explícitamente todos los valores disponibles. Las notaciones abreviadas más abusadas son:

- padding
- margin
- font
- background
- border
- border-radius

A veces no es necesario declarar todos los valores. Por ejemplo, los encabezados HTML solo establecen `top-margin` y `bottom-margin`; entonces, cuando sea necesario, solo sobreescribe estos dos valores. Su uso excesivo normalmente lleva a código con _overrides_ innecesarios y efectos secundarios no deseados.

### Mal ejemplo ❌
```css
.element {
  margin: 0 0 10px;
  background: red;
  background: url("image.jpg");
  border-radius: 3px 3px 0 0;
}
```

### Buen Ejemplo ✅
```css
.element {
  margin-bottom: 10px;
  background-color: red;
  background-image: url("image.jpg");
  border-top-left-radius: 3px;
  border-top-right-radius: 3px;
}
```

## _Nesting_ en LESS and SASS

Evita el _nesting_ innecesario. Solo porque puedas _nestear_ no significa que siempre debas hacerlo. _Nestea_ solo cuando debes _scopear_ estilos a un padre y si hay múltiples elementos a _nestear_.

```css
// Sin nesting
.table > thead > tr > th { … }
.table > thead > tr > td { … }

// Con nesting
.table > thead > tr {
  > th { … }
  > td { … }
}
```

## Operadores en LESS y SASS

Para mayor legibilidad, enciera las operaciones matemáticas en paréntesis con un espacio entre los valores, variables y operadores.

### Mal ejemplo ❌ 
```css
.element {
  margin: 10px 0 @variable*2 10px;
}
```

### Buen Ejemplo ✅
```css
.element {
  margin: 10px 0 (@variable * 2) 10px;
}
```

## Comentarios

El código es escrito y mantenido por gente. Asegúrate que tu código es descriptivo, está bien comentado y es fácil de entender por otros. Los mejores comentarios de código dan contexto o propósito. No simplemente repitas el nombre de un componente o clase.

Asegúrate de escribir enunciados completos para comentarios largos y frases de notas generales.

### Mal ejemplo ❌
```css
/* Modal header */
.modal-header {
  ...
}
```

### Buen Ejemplo ✅
```css
/* Wrapping element for .modal-title and .modal-close */
.modal-header {
  ...
}
```


## Nombres de clases

Mantén las clases en minúsculas y usa guiones (no guión bajo ni camelCase). Los guiones sirven como divisores naturales en clases relacionadas (ejemplo: `.btn` y `.btn-danger`)

Evita el uso excesivo y arbitrario de abreviaciones. `.btn` es útil para un botón, pero `.s` no significa nada.

Mantén el nombre de las clases tan corto y sucinto como sea posible.

Una nombres significativos; usa nombres estructurales o de propósito sobre presentacionales.

Pónle prefijo a las clases basado en el padre más cercano o la clase base.

Usa clases `.js-*` para denotar comportamiento (en contraste con estilo) pero mantén estas clases fuera de tu CSS.

También es útil aplicar muchas de estas reglas al nombrar variables en SASS y LESS.

### Mal ejemplo ❌
```css
.t { ... }
.red { ... }
.header { ... }
```

### Buen Ejemplo ✅
```css
.tweet { ... }
.important { ... }
.tweet-header { ... }
```


## Selectores

Usa clases sobre _tag_ genéricos para un rendimiento óptimo.

Evita usar muchos selectores (ejemplo: `[class^="..."]`) en componentes comunes. El rendimiento puede verse afectado por esto.

Mantén los selectores cortos y limita el número de elementos en cada selector a un máximo de 3.

_Scopea_ las clases al padre más cercano solo cuando sea necesario (ejemplo: no cuando uses clases con prefijo).

### Mal ejemplo ❌
```css
span { ... }
.page-container #stream .stream-item .tweet .tweet-header .username { ... }
.avatar { ... }
```

### Buen Ejemplo ✅
```css
.avatar { ... }
.tweet-header .username { ... }
.tweet .avatar { ... }
```

## Organización

Organiza las secciones de tu código por componente.

Desarrolla una jerarquía de comentarios consistente.

Usa consistentemente los espacios a tu favor cuando separes secciones de código para facilitar el escaneo de documentos grandes.

Cuando uses muchos archivos CSS, sepáralos por componente en lugar de por página. Las páginas pueden re-acomodarse y los componentes, moverse.

```css
/*
 * Component section heading
 */

.element { ... }


/*
 * Component section heading
 *
 * Sometimes you need to include optional context for the entire component. Do that up here if it's important enough.
 */

.element { ... }

/* Contextual sub-component or modifer */
.element-heading { ... }
```
