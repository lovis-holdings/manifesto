# Pared de la Fama

Deja tu huella en la pared de la fama...

## Aldo Funes Minutti
> Mountains are not Stadiums where I satisfy my ambition to achieve, they are the cathedrals where I practice my religion.

`Cloud` `AWS` `Serverless` `DevOps` `CI` `JavaScript` `React` `GraphQL` `Redux` `Marketing` `MongoDB` `PostgreSQL` `Windows (doom)` `SQL Server`

Twitter: [@F_U_N_E_S](https://www.twitter.com/F_U_N_E_S)
Github: [@aldofunes](https://www.github.com/aldofunes)

-------
