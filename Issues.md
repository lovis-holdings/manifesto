# Issues

## Formato:

### Resumen del problema

Resumen del problema incluyendo el sistema operativo, browser, entorno y versiones con las cuales ocurrió el problema.

### Pasos para reproducir

1. Primer paso...
2. Segundo paso...

### Comportamiento esperado

¿Por qué esto es considerado un problema? ¿Cuál es el comportamiento esperado?

